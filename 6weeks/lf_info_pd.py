import requests, json
import openpyxl as op
import pandas as pd

url = "https://mapi.lfmall.co.kr/api/search/v2/outlet/categories"

data = json.dumps({
    "aggs": [
        "saleType"
        , "colors"
        , "season"
        , "styleYear"
        , "brandGroup"
        , "gender"
        , "searchCategory"
        , "price"
        , "prop1"
        , "prop2"
        , "size1"
        , "size2"
        , "benefit"
        , "review"
        ],
    "order": "popular",
    "page" : 1,
    "pid": [2080104],
    "size": 40,
    "tid": [2080047]
    })

headers = {
    "Content-Type": "application/json; charset=UTF-8"
}

response = requests.post(url=url, data=data, headers=headers)

if response.status_code == 200:

    my_info = []
    for i in response.json()["results"]["products"]:
        my_info.append({
            "brandName": i["brandName"],
            "name": i["name"],
            "price": i["originalPrice"]
        })

    df = pd.DataFrame(my_info)

    df.to_excel("./lf_mall_info_pd.xlsx", index=False)
    print("[success] make excel")
else:
    print(response.status_code + " error")
    print("[fail] make excel")