import requests, json
import openpyxl as op

url = "https://mapi.lfmall.co.kr/api/search/v2/outlet/categories"

data = json.dumps({
    "aggs": [
        "saleType"
        , "colors"
        , "season"
        , "styleYear"
        , "brandGroup"
        , "gender"
        , "searchCategory"
        , "price"
        , "prop1"
        , "prop2"
        , "size1"
        , "size2"
        , "benefit"
        , "review"
        ],
    "order": "popular",
    "page" : 1,
    "pid": [2080104],
    "size": 40,
    "tid": [2080047]
    })

headers = {
    "Content-Type": "application/json; charset=UTF-8"
}

response = requests.post(url=url, data=data, headers=headers)

if response.status_code == 200:
    my_info = [["브랜드명", "상품명", "판매가격"]]

    for i in response.json()["results"]["products"]:
        my_info.append([
            i["brandName"],
            i["name"],
            i["originalPrice"]
        ])

    wb = op.Workbook()
    ws = wb.active
    ws.column_dimensions[op.utils.get_column_letter(1)].width = 25
    ws.column_dimensions[op.utils.get_column_letter(2)].width = 50

    for i, j in enumerate(my_info):
        for k, l in enumerate(j):
            wc = ws.cell(i+1, k+1, l)
            wc.alignment = op.styles.Alignment(horizontal="center", vertical="center")
            wc.border = op.styles.Border(left=op.styles.Side(style="thin"), right=op.styles.Side(style="thin"), 
		                                    top=op.styles.Side(style="thin"), bottom=op.styles.Side(style="thin"))
            wc.font = op.styles.Font(name="맑은 고딕")
            if i == 0:
                wc.font = op.styles.Font(bold=True, name="맑은 고딕")
                wc.fill = op.styles.PatternFill(fgColor="00339966", fill_type="solid")
                
    wb.save("lf_mall_info_op.xlsx")
    print("[success] make excel")
else:
    print(response.status_code + " error")
    print("[fail] make excel")