import argparse


def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("items", nargs="+")
    parser.add_argument("--sorted", action="store_true")
    return parser.parse_args()


def main():
    args = get_args()
    args.items = sorted(list(dict.fromkeys(args.items))) if args.sorted else list(dict.fromkeys(args.items))

    if len(args.items) > 1:
        for i, v in enumerate(args.items):
            print( " and " + v if len(args.items) == i+1 else ", " + v if i != 0 else v, end = '')
    else:
        print(args.items[0])
    

if __name__ == "__main__":
    main()