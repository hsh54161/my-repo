import argparse


def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("string")
    parser.add_argument("top_number")
    return parser.parse_args()


def main():
    args = get_args()
    count = {}
    alpha_str = args.string.lower()
    top_num = int(args.top_number)

    for i in range(97, 123):
        if alpha_str.count(chr(i)) > 0:
            count[chr(i)] = alpha_str.count(chr(i))

    count = dict(sorted(count.items(), key = lambda item: item[1], reverse=True))

    for k, v in count.items():
        if top_num <= 0:
            break
        else:
            print(f"{k} {v}")
        top_num -= 1


if __name__ == "__main__":
    main()