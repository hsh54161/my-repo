from Athlete import *


class Shooter(Athlete):

    population = 0

    def __init__(self, name, stamina):
        Athlete.__init__(self, name, stamina)
        Shooter.population += 1

    def shoot(super):
        if super.stamina >= 5:
            print("Shoot.")
            super.stamina -= 5
        else:
            print("Can't shoot.")

    def run(super):
        print("I hate running.")