from Athlete import *


class SoccerPlayer(Athlete):

    population = 0

    def __init__(self, name, stamina):
        Athlete.__init__(self, name, stamina)
        SoccerPlayer.population += 1

    def kick(super):
        if super.stamina >= 20:
            print("kick.")
            super.stamina -= 20
        else:
            print("Can't kick.")