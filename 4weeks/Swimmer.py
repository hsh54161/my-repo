from Athlete import *


class Swimmer(Athlete):

    population = 0

    def __init__(self, name, stamina):
        Athlete.__init__(self, name, stamina)
        Swimmer.population += 1

    def swim(super):
        if super.stamina >= 30:
            print("Swim.")
            super.stamina -= 30
        else:
            print("Can't swim.")