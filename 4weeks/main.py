from SoccerPlayer import *
from Shooter import *
from Swimmer import *


def main():
    soccer_player1 = SoccerPlayer(name="John", stamina=30)
    soccer_player2 = SoccerPlayer(name="Ann", stamina=20)
    swimmer = Swimmer(name="Jane", stamina=40)
    shooter = Shooter(name="Michael", stamina=10)

    soccer_player1.introduce()
    soccer_player2.introduce()
    swimmer.introduce()
    shooter.introduce()

    print()

    soccer_player1.run()
    soccer_player1.kick()
    soccer_player1.kick()
    soccer_player1.rest()
    soccer_player1.kick()

    print()

    soccer_player2.run()
    soccer_player2.kick()
    soccer_player2.rest()
    soccer_player2.kick()

    print()

    swimmer.run()
    swimmer.swim()
    swimmer.swim()
    swimmer.rest()
    swimmer.swim()

    print()

    shooter.run()
    shooter.shoot()
    shooter.shoot()
    shooter.shoot()
    shooter.rest()
    shooter.shoot()

    print()

    print(SoccerPlayer.population)
    print(Swimmer.population)
    print(Shooter.population)


if __name__ == "__main__":
    main()
