class Athlete:
    def __init__(self, name, stamina):
        self.name = name
        self.org_stamina = stamina
        self.stamina = stamina
    def introduce(self):
        print("My name is " + self.name)
    
    def run(self):
        if self.stamina >= 10:
            print("Run.")  
            self.stamina -= 10
        else:
            print("Can't run.")
            
    def rest(self):
        self.stamina = self.org_stamina
        print("Rest.")
